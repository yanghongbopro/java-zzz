import com.yhb.Application;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = Application.class)
public class UserTest {


    @Test
    public void test1(){
        int a = 1;
        int b = 2;
        int c = a + b;
        Assert.assertEquals(3,c);
    }
}
